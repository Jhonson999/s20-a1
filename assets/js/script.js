// *
  
//   Create an arrow function called postCourse which allows
//   us to add a new object into the array. It should receive data: id, name, description
//   price, isActive.

//   Add the new course into the array and show the following alert:
//    "You have created <nameOfCourse>. The price is <priceOfCourse>."

//    Create an arrow function wchich allows us to find a particular course providing the course id and return the details
//    of the found course.
//     - use find()

//    Create an arrow function called deleteCourse which can delete the laast course object in the array.
//    -pop() 
// */

let courseArray = [];
   

const postCourse = (id, name, description, price, isActive) => {
   courseArray.push({
        id: id,
        name: name,
        description: description,
        price: price,
        isActive: isActive

 })

   alert(`You have created ${name}. The price is ${price}.`);

 
  };

postCourse("105","Heat and Mass Transfer", "All about transfer of heat.","$199.99",true);
postCourse("101","Refrigeration and Air Conditioning","About air-conditioning and refrigeration systems.","$299.99",true);
postCourse("201","Fluid Mechanics","Study of mechanics of fluids and the forces on them.","$259.99",true);


console.log(courseArray);

let courseID = prompt('Enter ID to find course:' );
const foundCourse = courseArray.find(foundCourse => foundCourse.id === courseID)?'available':'not available'
  alert(`This course is ${foundCourse}.`);



const deleteLastCourse = () => courseArray.pop();
deleteLastCourse();
console.log(courseArray);
